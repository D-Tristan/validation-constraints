package net.tncy.tde.validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ISBNValidatorTest
{
    private static ISBNValidator iSBNvalidator;
    private static Validator validator;

    @BeforeClass
    public static void setUpClass()
    {
        iSBNvalidator = new ISBNValidator();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testNull() throws Exception
    {
        isValid(null);
    }

    @Test
    public void testValid() throws Exception
    {
        isValid("134");
    }

    @Test
    public void testInvalidIBAN() throws Exception
    {
        isNotValid("12345AZERTY");
        isNotValid("BE43068999999509123");
    }

    private void isValid(String bic)
    {
        assertTrue("Expected a valid ISBN.", iSBNvalidator.isValid(bic, null));
    }

    private void isNotValid(String bic)
    {
        assertFalse("Expected an invalid ISBN.", iSBNvalidator.isValid(bic, null));
    }
}
