package net.tncy.tde.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String>
{
    @Override
    public void initialize(ISBN constraintAnnotation)
    {
        // Ici le validateur peut accéder aux attribut de l’annotation.
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext)
    {
        boolean valid = true;

        if (bookNumber != null && bookNumber.length() > 5)        // Algorithme de validation du numéro ISBN
        {
            valid = false;
        }

        return valid;
    }
}